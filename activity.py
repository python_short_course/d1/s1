name = "Jimmy"
age = 25
occupation = "Web Developer"
movie = "By the Grace of Gods"
rating = 99.9

print(f"I am {name}, and I am {age}, years old, I work as a {occupation}, and my rating for {movie} is {rating}%")

num1, num2, num3 = 20,15,25

print(num1 * num2)
print(num1 < num3)
print(num3 + num2)